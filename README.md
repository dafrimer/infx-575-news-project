# Jevin's Bullshit Detector

This repository contains code, data, etc. for our INFX 575 final project. It's
private on GitHub right now and probably should stay that way, since it includes
data
from [this source](https://github.com/GeorgeMcIntire/fake_real_news_dataset)
which has no associated license.


